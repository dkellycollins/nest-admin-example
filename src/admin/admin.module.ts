import { AdminModule as _AdminModule } from '@adminjs/nestjs';
import { DynamicModule } from '@nestjs/common';
import AdminJS, { ResourceWithOptions } from 'adminjs';
import { resolve } from 'path';

export interface AdminModuleOptions {
  rootPath?: string;
  resources?: Array<ResourceWithOptions>;
}

console.log(process.env.NODE_ENV);

export class AdminModule {
  public static register(options: AdminModuleOptions): DynamicModule {
    return _AdminModule.createAdmin({
      adminJsOptions: {
        rootPath: options.rootPath,  
        resources: options.resources,
        dashboard: {
          component: AdminJS.bundle('./dashboard.component')
        }
      }
    });
  }
}