import { Module } from '@nestjs/common';
import { AdminModule } from './admin/admin.module';

@Module({
  imports: [
    AdminModule.register({ resources: [] })
  ]
})
export class AppModule {}
